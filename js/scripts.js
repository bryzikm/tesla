$(document).ready(function () {

    setTimeout(function () {
        homepageHistoryColumnHeight();
    }, 100);

    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 10) {

            $('#header').addClass('fixed');
        }
        else {
            $('#header').removeClass('fixed');
        }
    });

    $(window).on('resize', function () {
        homepageHistoryColumnHeight();
    });

    $('#mobile').on('click', function (e) {
        e.preventDefault();

        if ($('#mobile').hasClass('clicked')) {
            $('#mobile').removeClass('clicked');
            $('#header .header--menu').css('display', 'none');
        }
        else {
            $('#mobile').addClass('clicked');
            $('#header .header--menu').css('display', 'block');
        }
    });

    function homepageHistoryColumnHeight() {
        if($('section.history--home').length && ($(window).width() > 767)) {
            var leftColumnHeight = $('section.history--home .history--content .column--left').height();

            $('section.history--home .history--content .column--right').css('height', leftColumnHeight);

        }
    }
});