<?php
/*
    Template Name: Contact
*/
?>
<?php get_header(); ?>
<section class="herobanner herobanner--contact">
    <div class="container">
        <h1 class="herobanner--header header">Kontakt</h1>
    </div>
</section>
<section class="content--contact content">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 column--left column">
                <h2>Informacje o firmie</h2>
                <p>Jeśli są Państwo zainteresowani naszą ofertą, bądź chcieliby Państwo uzyskać szczegółowe informacje
                    na temat oferowanych przez nas usług, gorąco zachęcamy do kontaktu telefonicznego, poprzez e-mail
                    lub osobiście w siedzibie naszej firmy.</p>
                <div id="map"></div>
                <p>
                    <span>Telsa-Trans Górecki sp.j</span><br>
                    NIP: 6372197522<br>
                    Łobzów 99<br>
                    32-340 Wolbrom<br><br>
                    tel.: 32 644 28 46<br>
                    e-mail: <a href="mailto:biuro@telsa-trans.com">biuro@telsa-trans.com</a>
                </p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 column--right column">
                <h2>Formularz kontaktowy</h2>
                <form>
                    <div class="form-group">
                        <label>Imię i nazwisko (wymagane)</label>
                        <input type="text" placeholder="Jan Kowalski" />
                    </div>
                    <div class="form-group">
                        <label>Adres email (wymagane)</label>
                        <input type="email" placeholder="kowal@gmail.com" />
                    </div>
                    <div class="form-group">
                        <label>Temat</label>
                        <input type="text" placeholder="Transport" />
                    </div>
                    <div class="form-group">
                        <label>Treść wiadomości</label>
                        <textarea placeholder="Witam! Czy jest możliwość..."></textarea>
                    </div>
                    <button type="submit">Wyślij</button>
                </form>
            </div>
        </div>
    </div>
</section>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUgjspAGc2B58T5Xxyb8iOc5R0ZFuaENo&callback=initMap"
        type="text/javascript"></script>
<script type="text/javascript">
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 50.4082809, lng: 19.7819732},
            zoom: 14,
            draggable: false,
            scrollwheel: false,
            styles: [
                {
                    "stylers": [
                        {
                            "hue": "#2c3e50"
                        },
                        {
                            "saturation": 250
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "lightness": 50
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                }
            ]
        });

        var marker = getMarker(50.4082809, 19.7819732);

        marker.setMap(map);
    }

    function getMarker(x, y) {
        return new google.maps.Marker({
            position: new google.maps.LatLng(x, y)
        });
    }

    initMap();
</script>
<?php get_footer(); ?>