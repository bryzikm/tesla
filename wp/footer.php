<?php wp_footer(); ?>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <span>© Telsa Trans 2017</span>
            </div>
            <div class="col-xs-6 text-right">
                <span><a href="http://bryzikm.pl" target="_blank">bryzikm.pl</a></span>
            </div>
        </div>
    </div>
</footer>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-2.2.4.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js" type="text/javascript"></script>
<script>
    wow = new WOW(
            {
                mobile: false
            }
    );
    wow.init();
</script>
</body>
</html>