<?php wp_head(); ?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title><?php the_title(); ?></title>

    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-theme.min.css" type="text/css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="index.html"><img src="img/logo.png" /></a>
            </div>
            <div class="col-xs-6 visible-xs">
                <a href="#" id="mobile">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 header--menu menu">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="services.html">Oferta</a></li>
                    <li><a href="contact.html">Kontakt</a></li>
                    <li class="phone--menu phone"><i class="fa fa-phone"></i><div><span>tel. +48696 178 436</span></div></li>
                </ul>
            </div>
        </div>
    </div>
</header>