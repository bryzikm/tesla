<?php
/*
    Template Name: Home
*/
?>
<?php get_header(); ?>
<section class="herobanner herobanner--home">
    <div class="container">
        <h1 class="herobanner--header header">Telsa-Trans Górecki Spółka Jawna</h1>
    </div>
</section>
<section class="about about--home">
    <div class="container">
        <div class="about--header header">
            <h2>O nas</h2>
        </div>
        <div class="row section--about">
            <div class="col-md-6 col-sm-6 col-xs-12 column column--image pull-right">
                <img src="img/about-1.jpg" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 column column--text">
                <div>
                    <p>Swoją współpracę z Klientami opieramy na solidnych fundamentach takich jak <span>niezawodność, terminowość,
                            staranność, elastyczność</span> oraz <span>profesjonalizm</span>. Jesteśmy doświadczoną firmą logistyczną i spedycyjną.
                        Realizowane przez nas zlecenia są w pełni dostosowane do potrzeb naszych Klientów.</p>
                </div>
            </div>
        </div>
        <div class="row section--about">
            <div class="col-md-6 col-sm-6 col-xs-12 column column--image">
                <img src="img/about-2.jpg" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 column column--text">
                <div>
                    <p>Każdy ładunek jest objęty <span>pozycjonowaniem GPS</span> oraz <span>ubezpieczeniem,</span>
                        pragniemy bowiem zapewnić wysoki standard świadczonych usług oraz dać Państwu pewność, że
                        powierzone zadanie trafiło w ręce profesjonalistów.</p>
                </div>
            </div>
        </div>
        <div class="row section--about">
            <div class="col-md-6 col-sm-6 col-xs-12 column column--image pull-right">
                <img src="img/about-3.jpg" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 column column--text">
                <div>
                    <p><span>Terminowość</span> ma dla nas kluczowe znaczenie. Wiemy, jak istotną rolę pełni ona w
                        dzisiejszym biznesie. W każdym momencie mogą Państwo polegać na wieloletnim
                        <span>doświadczeniu i umiejętnościach</span> naszej wykwalifikowanej kadry.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="history history--home">
    <div class="container">
        <div class="row">
            <div class="history--header header">
                <h2>Historia</h2>
            </div>
        </div>
        <div class="row history--content content">
            <div class="col-md-6 col-sm-6 col-xs-12 column column--left">
                <div class="block block--history">
                    <img src="img/history-flag.png" />
                    <p>Początki naszej działalności sięgają <span>1991 roku</span>, kiedy została zarejestrowana jednoosobowa
                        działalność gospodarcza pod nazwą Transport Towarów Augustyn Górecki. Głównym, przedmiotem
                        naszej działalności były usługi w transporcie krajowym. </p>
                </div>
                <div class="block block--history">
                    <img src="img/history-car.png" />
                    <p>W <span>2016 r.</span> postanowiliśmy zmienić formę prawną naszej działalności i powołaliśmy do życia Telsa –
                        Trans Górecki Sp. J., rozszerzając jednocześnie naszą ofertę o usługi spedycyjne.</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 column column--right">
                <div class="block block--history">
                    <img src="img/history-earth.png" />
                    <p>W roku <span>1994</span> uzyskaliśmy koncesję na przewóz rzeczy w międzynarodowym transporcie drogowym. Od tej
                        pory działamy również na arenie międzynarodowej. Od początku naszej działalności wykonywaliśmy
                        transporty na terenach takich krajów jak: <span>Węgry, Słowacja, Czechy, Włochy, Rumunia, Chorwacja,
                        Austria, Litwa, Łotwa, Estonia, Szwecja, Holandia</span></p>
                </div>
            </div>
            <div class="history--line line">
                <span class="dot dot--big"></span>
                <div class="line--dotted"></div>
                <span class="dot dot--small"></span>
                <div class="line--dotted"></div>
                <span class="dot dot--small"></span>
                <div class="line--dotted"></div>
                <span class="dot dot--big"></span>
            </div>
        </div>
        <div class="row history--summary summary">
            <p><span>Obecnie</span> działamy zarówno jako Transport Towarów Augustyn Górecki jak i Telsa – Trans Górecki sp. j.</p>
        </div>
    </div>
</section>
<section class="info--home info">
    <div class="container">
        <div class="row info--header header">
            <h2 class="col-xs-12">Chcesz wiedzieć więcej?</h2>
        </div>
        <div class="row info--content content">
            <div class="col-xs-12">
                <p class="paragraph--big paragraph">
                    Jeśli są Państwo zainteresowani naszą ofertą, bądź chcieliby Państwo uzyskać szczegółowe informacje na
                    temat oferowanych przez nas usług, gorąco zachęcamy do kontaktu telefonicznego, poprzez e-mail lub
                    osobiście w siedzibie naszej firmy.
                </p>
                <a href="#" class="button--blue button">Skontaktuj się z nami</a>
                <p class="paragraph paragraph--small">
                    Nasi Pracownicy szczegółowo udzielą Państwu wyczerpujących informacji. Jesteśmy do Państwa
                    dyspozycji o każdej porze.
                </p>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>