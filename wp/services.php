<?php
/*
    Template Name: Services
*/
?>
<?php get_header(); ?>
<section class="herobanner herobanner--services">
    <div class="container">
        <h1 class="herobanner--header header">Oferta</h1>
    </div>
</section>
<section class="services--content content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 column--single column">
                <p>Strategia naszej firmy opiera się przede wszystkim na budowaniu długoletniej współpracy z naszymi
                    Klientami. Dbając o ich interesy oraz o najwyższą jakość naszych usług, każdego Kontrahenta
                    traktujemy indywidualnie. Odpowiadając na potrzeby naszych klientów podejmujemy się różnych,
                    wymagających zadań, dostosowując również sprzęt oraz kwalifikacje kierowców do warunków
                    Zleceniodawców.</p>
                <p>Na chwilę obecną dysponujemy następującym taborem samochodowym:</p>
            </div>
            <div class="services--images images">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image--single image wow fadeInLeft">
                    <img src="img/service-1.png" />
                    <p>Nowoczesne ciągniki siodłowe</p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image--single image wow fadeInUp">
                    <img src="img/service-2.png" />
                    <p>Naczepy typu chłodnia - double deck</p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image--single image wow fadeInUp">
                    <img src="img/service-3.png" />
                    <p>Naczepy typu izoterma - double deck</p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image--single image wow fadeInRight">
                    <img src="img/service-4.png" />
                    <p>Naczepy typu firanka</p>
                </div>
            </div>
            <div class="col-xs-12 column--single column">
                <p>Każda z wyżej wymienionych naczep posiada <span>ładowność 24 ton</span> i kubaturę <span>85 – 95 m3 (34 europalety).</span>
                    Wszystkie zespoły pojazdów posiadają wyposażenie do przewozu materiałów niebezpiecznych ADR.
                    Ponadto samochody są wyposażone w <span>system GPS.</span></p>
                <p>Zatrudniamy również <span>wysoko wykwalifikowaną kadrę pracowników,</span> przeszkolonych w zakresie
                    przewozu materiałów niebezpiecznych ADR. Kierowcy są <span>profesjonalni i godni zaufania,</span> co
                    znajduje potwierdzenie w fakcie, iż każdy z nich jest zarejestrowany w międzynarodowym systemie DIDB
                    oraz posiada kartę DIDB (tzw. Karta zaufanego kierowcy).</p>
                <p>Oprócz własnego taboru samochodów posiadamy również rejestr zaufanych i zweryfikowanych firm transportowych
                    specjalizujących się w transporcie krajowym oraz międzynarodowym. Dzięki temu jesteśmy w stanie
                    zapewnić bezpieczny transport dla większej ilości zamówień. Gwarantujemy <span>solidność usług,
                    rzetelność i elastyczność</span> we współpracy oraz <span>konkurencyjne ceny.</span></p>
                <p>Zapraszamy do współpracy.</p>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>